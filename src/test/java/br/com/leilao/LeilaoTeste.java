package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    private Usuario usuario1;
    private Usuario usuario2;
    private Lance lanceMaior;
    private Lance lanceMenor;
    private Leilao leilao = new Leilao();
    private Leiloeiro leiloeiro = new Leiloeiro("João Leilões", leilao);

    @BeforeEach
    public void setUp(){
        this.usuario1 = new Usuario(1, "Bruno");
        this.usuario2 = new Usuario(2, "Patricia");
        this.lanceMenor = new Lance(usuario1, 100.00);
        this.lanceMaior = new Lance(usuario2, 150.00);
    }

    @Test
    public void testarAdicionarUmLance(){
        leilao.adicionarLance(lanceMenor);
        Assertions.assertEquals(true, leilao.getLances().contains(lanceMenor));
    }

    @Test
    public void testarUmLanceComValorMenor(){
        leilao.adicionarLance(lanceMaior);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarLance(lanceMenor);});
    }

    @Test
    public void testarUmLanceComValorMaior(){
        leilao.adicionarLance(lanceMenor);
        leilao.adicionarLance(lanceMaior);

        Assertions.assertEquals(lanceMaior.getValorDoLance(), leiloeiro.retornarMaiorLance().getValorDoLance());
    }

    @Test
    public void testarMaiorLanceDoLeilao(){
        leilao.adicionarLance(lanceMenor);
        leilao.adicionarLance(lanceMaior);

        Assertions.assertEquals(lanceMaior.getValorDoLance(), leiloeiro.retornarMaiorLance().getValorDoLance());
        Assertions.assertEquals(lanceMaior.getUsuario(), leiloeiro.retornarMaiorLance().getUsuario());
    }
}
