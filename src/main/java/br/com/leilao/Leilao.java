package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances = new ArrayList<>();
    private Lance maiorLance = new Lance();

    public Leilao(){}

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance getMaiorLance() {
        return maiorLance;
    }

    public void setMaiorLance(Lance maiorLance) {
        this.maiorLance = maiorLance;
    }

    public void adicionarLance(Lance lance) {
        if(validarLance(lance)){
            lances.add(lance);
            maiorLance.setValorDoLance(lance.getValorDoLance());
            maiorLance.setUsuario(lance.getUsuario());
        }
    }

    public boolean validarLance(Lance lance) {
       if(lance.getValorDoLance() < maiorLance.getValorDoLance()){
           throw new RuntimeException("Valor do lance menor que o último lance");
       } else {
           return true;
       }
    }
}
